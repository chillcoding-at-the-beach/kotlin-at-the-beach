# kotlin-at-the-beach

Projet de la formation kotlin

## Formation Kotlin : 100% Langage Kotlin en 3 jours

### Jour 1 : Fondamentaux Kotlin

1. INTRODUCTION À KOTLIN </> [TD : Environnement de développement](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_1-environnement_developpement_kotlin.html)
2. BASES DE KOTLIN </> [TD / TP : Kotlin : variable, opérateur, condition](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_2-base_kotlin.html)
3. CLASSES EN KOTLIN </> [TD / TP : Kotlin : Classe, Propriété, Fonction](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_3-classe_kotlin.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/tree/main/src/main/kotlin/agua/aqua)
4. PROGRAMMATION FONCTIONNELLE </> [TD / TP : Kotlin : Fonction à fond](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_4-fonction_kotlin.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/blob/main/src/main/kotlin/agua/beach/Fun.kt)

### Jour 2 : Kotlin Programmation Fonctionnelle et Orientée Objet
4. PROGRAMMATION FONCTIONNELLE </> Kotlin : Fonction d'extension et Lambda 
5. DÉLÉGATION, GÉNÉRIQUE </> [TD / TP : Kotlin : Délégation, Générique](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_5-delegation_generique_kotlin.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/blob/main/src/main/kotlin/agua/beach/BeachEquipment.kt)
6. KOTLIN AVANCÉ, BIBLIOTHÈQUE STD </> [TD / TP : Kotlin Avancé](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_6-kotlin_avance.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/blob/main/src/main/kotlin/agua/beach/BeachBag.kt)

### Jour 3 : Kotlin Avancé
7. INTEROPÉRABILITÉ </> [TD / TP : Kotlin : Intéropérabilité avec Java](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_7-java_kotlin.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/tree/main/src/main/kotlin/agua/java)
8. PROGRAMMATION ASYNCHRONE </> [TD / TP : Coroutine Kotlin](https://www.chillcoding.com/app/kotlin-for-android/challenge2/2_8-coroutine_kotlin.html), [code src](https://gitlab.com/chillcoding-at-the-beach/kotlin-at-the-beach/-/blob/main/src/main/kotlin/agua/crab/Race.kt)

Les bonnes pratiques Kotlin sont vues à travers ces 8 paries.

### Application Fil Rouge
Autour de l'univers de la plage
Application de Plage _Magic Beach_

#### Terminology
_beach, length of bridge, bridgeLength, towel, fish, deck, bubble, aquarium, tower tank, fish, beach party_



#### Références
[m2iFormation : Formation Conception et langages Kotlin - Mise en oeuvre](https://www.m2iformation.fr/formation-kotlin-mise-en-oeuvre/KOTL-FND/)

[PLB : Formation sur le Langage Kotlin](https://www.plb.fr/formation/developpement/formation-langage-kotlin,13-700949.php)

[developer.android.com: android-development-with-kotlin](https://developer.android.com/courses/android-development-with-kotlin/unit-1)

[ChillCoding.com : Formation 100% Langage Kotlin](https://macha-s-school-1ff9.thinkific.com/courses/100-kotlin)
