import agua.aqua.*
import agua.fish.Plecostomus
import agua.fish.RedFish
import agua.fish.Shark

fun buildBoat() {
    val boat = Boat()
    boat.printSize()
    boat.floor = 60
    boat.printSize()

    val boat1 = Boat(width = 30)
    boat1.printSize()

    val boat2 = Boat(length = 80)
    boat2.printSize()

    val boat3 = Boat(50, 30, 40)
    boat3.printSize()

    val boat4 = Boat(29)
    boat4.printSize()
    boat4.area = 70
    boat4.printSize()

    val myTower = SailBoat(40, 25)
    myTower.printSize()
}

fun makeFish() {
    val shark = Shark()
    val pleco = Plecostomus()
    println("Shark : ${shark.color}")
    shark.eat()
    println("Pleco : ${pleco.color}")
    pleco.eat()
    val fish = RedFish()
    println("Fish : ${fish.color}")
    fish.eat()
}

fun main(args: Array<String>) {
    makeFish()
}