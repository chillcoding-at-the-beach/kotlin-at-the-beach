package agua.beach

/** 6
 *
 * Cette classe représente un sac de plage.
 * Sa mission :
 *  -> mettre en évidence les valeurs multiple
 *
 */

data class BeachBag(
    val pocket1: String,
    val pocket2: String,
    val pocket3: String = "poc3",
    val pocket4: String = "poc4"
)

class Bag(val p1: String = "p1", val p2: String = "p2", val p3:String="p3"){
    operator fun component1():String = p1
    operator fun component2():String = p2
    operator fun component3():String = p3
}

fun returnBagThings(): BeachBag {
    val word = "abcd"
    val strId = word.reversed()
    val number = (0..100).random().toString()
    return BeachBag(strId, number)
}

fun main() {
    val data = returnBagThings()
    println("POCKET 1 : ${data.pocket1}  POCKET 2: ${data.component2()}")

    val (poc1, poc2, poc3, poc4) = returnBagThings()
    println("POC 1: $poc1  POC 2: $poc2 POC 3: $poc3 POC 4 $poc4")

    val (a , b, c) = Bag()
    println("Bag $a $b $c")
}