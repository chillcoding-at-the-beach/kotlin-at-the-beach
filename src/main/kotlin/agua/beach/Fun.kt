package agua.beach

import kotlin.random.Random

val beachArray = arrayOf(
    "Plage des Rochers",
    "Praia de Carcavelos",
    "Playa de Barceloneta",
    "Page des ondes",
    "Copacabana",
    "Ipanema",
    "Bahamas"
)

val pontoon = listOf(3, 7, 9, 13, 21, 25)

fun showAquarium() = println("This is an aquarium")

fun randomBeach(): String {
    return beachArray[Random.nextInt(beachArray.size)]
}

fun whereIsAquarium(aquaId: Int): String {
    return when (aquaId) {
        1 -> beachArray[3]
        2 -> beachArray[0]
        3 -> beachArray[5]
        4 -> beachArray[4]
        5 -> beachArray[1]
        else -> beachArray[2]
    }
}

fun String.hasSpaces(): Boolean {
    val found = this.indexOf(' ')
    return found != -1
}

fun main() {
    val isUnit = showAquarium()
    println(isUnit)

    val temperature = 10
    // val isHot = if (temperature > 50 ) true else false
    val isHot = temperature > 50
    println(isHot)

    println("Random Beach ${randomBeach()}")

    println(whereIsAquarium(3))

    var cleanLevel = 10
    val waterFilter: (Int) -> Int = { clean: Int -> clean / 2 }
    println(waterFilter(cleanLevel))

    println("toto".hasSpaces())
    println("to to ".hasSpaces())

    println(beachArray.flatMap { it.toList() })

    println(pontoon.average())

    println(pontoon.maxOrNull())

    println(pontoon.minOrNull())

    println(beachArray.count())

    println(beachArray.size)

    println(pontoon.count { it > 10})

    println(pontoon.drop(3))
}