package agua.beach

open class BeachEquipment(val name: String) {
    fun removeSand() {}
}

data class Towel(val towelName: String) : BeachEquipment(towelName)

data class Umbrella(val umbrellaName: String) : BeachEquipment(umbrellaName)

fun removeSand(equipment: List<BeachEquipment>) {
    equipment.forEach {
        it.removeSand()
        println(it.name + "cleaned!")
    }
}

val comparedByNames = Comparator { a:BeachEquipment, b:BeachEquipment ->
    a.name.length - b.name.length
}

fun main() {
    val umbrellaList = listOf(Umbrella("Yellow Umbrella"), Umbrella("Beach Umbrella"), Umbrella("Pequino Umbrella"))
    val towelList = listOf(Towel("Red Towel"), Towel("circle towel"))
    val beachParty = umbrellaList + towelList
    print(beachParty)
    removeSand(beachParty)
    val beachPartyTried = beachParty.sortedWith(comparedByNames)
    println(beachPartyTried)
}

