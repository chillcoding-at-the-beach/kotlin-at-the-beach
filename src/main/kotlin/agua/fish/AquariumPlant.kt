package agua.fish

data class AquariumPlant(val color: String, private val size: Int)

fun AquariumPlant.isRed() = color == "red"

//fun AquariumPlant.isBig() = size > 50

val AquariumPlant.isGreen: Boolean
    get() = color == "green"

fun AquariumPlant?.pull () {
    this?.apply {
        println("removing $this")
    }
}

fun makeAquaPlant() {
    val plant = AquariumPlant("yellow", 33)
    println(plant.isRed())
    val greenPlant = AquariumPlant("red", 13)
    println(greenPlant.isGreen)

    val nullPlant = null
    println("not displaying the nullable")
    nullPlant.pull()
    println("Displaying the yellow one")
    plant.pull()
}

fun main() {
    makeAquaPlant()
}