package agua.fish

/** 3
 *
 * Cette classe représente un poisson d'aquarium.
 * Sa mission :
 *  -> mettre en évidence le concept de classe abstraite
 *  -> utiliser le concept d'interface
 *
 */
abstract class Fish : FishAction {
    abstract val color: String
    override fun eat() = println("yumº")
}

/** 3
 *
 * Cette classe représente une couleur de poisson.
 * Sa mission :
 *  -> mettre en évidence le concept d'interface
 *
 */
interface FishAction {
    fun eat()
}

class Shark : Fish(), FishAction {
    override val color = "grey"
    override fun eat() {
        println("<<<^^^^>>> ")
    }
}

class Plecostomus : Fish(), FishAction {
    override val color = "gold"
    override fun eat() {
        println("eat algae")
    }
}

class RedFish : Fish() {
    override val color = "red"
}