package agua.deco

fun main() {
    var alga: String? = null
    lateinit var fish: String
    val beach by lazy { fish.length }

    alga = "ALGUA"
    println(alga?.length ?: ";)")
    // ?.PROPRIETE : si c'est pas null
    // ?: VALUE : si c'est null
    alga!!.length

    fish = "Fish with $alga"

    println(fish)

    println(beach)
}