package agua.deco

/** 3
 *
 * Ces classes représente des décorations d'aquarium.
 * Leur mission :
 *  -> implémenter des classes de données
 *
 */
data class Decoration(val rocks: String)


/** 3
 *
 * Cette classe représente une direction.
 * Sa mission :
 *  -> implémenter une classe d'énumération
 *
 */
enum class Direction(val degrees: Int) {
    NORTH(0), SOUTH(180), EAST(90), WEST(270)
}

fun makeDeco() {
    val deco = Decoration("granite")
    val deco2 = Decoration("crystal")
    val deco3 = Decoration("crystal")
    println("AQUA DECO : $deco , $deco2")
    println(
        "deco 2 and deco 3 ? equals ${deco2.equals(deco3)}" +
                " == ${deco2 == deco3} " +
                "=== ${deco2 === deco3}"
    )
}

fun main() {
    println(Direction.EAST.name)
    println(" ${Direction.EAST.ordinal} S ${Direction.SOUTH.ordinal}")
    println(Direction.EAST.degrees)
}
