package agua.java

data class Palm(val height: Int = 20, val stem: Int = 5) {
    fun dim() = height * stem * 3
    companion object {
        @JvmStatic
        fun printPalm(): Unit {
            printStem()
            printTrunk()
        }
    }
}

fun printStem() {
    println("   |  ")
    println(")  | (")
    println(" ) | (")
    println("  ) (")
}

fun printTrunk(height: Int = 7) {
    repeat(height) {
        println("  | | ")
    }
    println("  _-_")
}