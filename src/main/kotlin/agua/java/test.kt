package agua.java


fun main() { /* inverse d'une suite de caractère dans un tableau par
       permutation des deux extrêmes */
    val Tablecar = charArrayOf('a', 'b', 'c', 'd', 'e', 'f')
    var i: Int
    var j: Int
    println("tableau avant : " + String(Tablecar))
    i = 0
    j = 5
    while (i < j) {
        var car: Char
        car = Tablecar[i]
        Tablecar[i] = Tablecar[j]
        Tablecar[j] = car
        i++
        j--
    }
    println("tableau après : " + String(Tablecar))
}
