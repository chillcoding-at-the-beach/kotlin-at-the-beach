package agua.crab

interface CrabAction {
    fun pinch()
}

interface GastropodShell {
    val shell: String
}

/** 3
 *
 * Cette classe représente un comportement de crabe.
 * Sa mision :
 *  -> mettre en évidence le concept de delegation
 *
 */
class PrintingCrabAction(val crabAction: String) : CrabAction {
    override fun pinch() {
        println(crabAction)
    }
}

object Shell : GastropodShell {
    override val shell = "((º))"
}

class Crab : CrabAction by PrintingCrabAction("_-<> P <>-_")

class HermitCrab : GastropodShell by Shell

class ShellCrab(val name: String) {
    var speed = true
    fun dance() = println("_-<> -___-<>")
    override fun toString() = "((-$name-))"
}

fun playWithCrab() {
    val crab = Crab()
    crab.pinch()

    val hermitCrab = HermitCrab()
    println(hermitCrab.shell)
}

fun main() {
    playWithCrab()
}
