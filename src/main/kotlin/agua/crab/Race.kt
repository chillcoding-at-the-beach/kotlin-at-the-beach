package agua.crab

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

suspend fun goToTheBeach(time: Long, crab: ShellCrab) {
    delay(time)
    println("${crab.name} is arrived at the beach.")
}

suspend fun chillGoToTheBeach(crab: ShellCrab) {
    println("${crab.name} is in starting bloc")
    if (crab.speed)
        delay(300L)
    else
        delay(1000L)
    crab.dance()
    delay(Random.nextLong(5000L))
    println("$crab is arrived at the beach.")
}


fun main() = runBlocking {
    val crab1 = ShellCrab("copacapana crab")
    val crab2 = ShellCrab("coco crab")
    val crab3 = ShellCrab("Invisible crab")
    val crab4 = ShellCrab("friend of Copacabana")

    println("Hermit crabs are going to the beach, see you there !")
    for (c in 5 downTo 0) print(" $c..")

    launch {
        goToTheBeach(500L, crab2)
        goToTheBeach(6000L, crab3)
    }

    val a = async {
        chillGoToTheBeach(crab1)
        true
    }

    if (a.await()) {
        launch {
            goToTheBeach(100L, crab4)
        }
    }

    println(" GO!")
}