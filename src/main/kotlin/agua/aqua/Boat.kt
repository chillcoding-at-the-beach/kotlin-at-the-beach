package agua.aqua

/** 3
 *
 * Cette classe représente un bateau.
 * Sa mission :
 *  -> montrer le concept de l'Héritage en Kotlin
 *  -> mettre en évidence les différents type de constructeur
 *
 */

open class Boat(open var length: Int = 10, open var width: Int = 5, open var floor: Int = 1) {
    init {
        println("Boat under construction  . . .")
    }

    open var area: Int
        get() = length * width * floor
        set(value) {
            floor = (value) / (length * width)
        }

    open val material = "wood"

    open var areaForPeople: Double = 0.0
        get() = area * 0.5

    constructor(numberOfPeople: Int) : this() {
        val area = numberOfPeople * 2
        floor = area / (length * width)
    }

    fun printSize() {
        println("The boat is in $material (material)")
        println("Length: $length meter ; Width: $width meter ; Floor: $floor ;")
        println("Area: $area m2; Area for People: $areaForPeople (${areaForPeople / area * 100.0} % full)")
    }
}

