package agua.aqua

class SailBoat(override var length: Int, override var width: Int) :
        Boat(length = length, width = width, floor = 2) {
    override var area: Int
        // area = width * length / 3
        get() = length * width / 3 * floor
        set(value) {
            floor = value / length / width
        }

    override var areaForPeople = area * 0.25

    override val material = "wood"
}